package com.utbm.lo52.chupachups;

public class XBeeReader {
	static {
		System.loadLibrary("xbeejni");
	}

	public native void initCOM();
	public native void closeCOM();
	public native int readChar();
}
