LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES := xbee_jni.c

LOCAL_C_INCLUDES += \
$(JNI_H_INCLUDE) \
$(LOCAL_PATH)/../xbee

LOCAL_MODULE := libxbeejni

LOCAL_MODULE_TAGS := optional

LOCAL_LDLIBS := -L$(SYSROOT)/usr/lib -llog

LOCAL_SHARED_LIBRARIES := \
libutils \
libcutils\
libxbee_shared

include $(BUILD_SHARED_LIBRARY)

