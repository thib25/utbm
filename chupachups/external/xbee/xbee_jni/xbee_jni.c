#include "jni.h"
#include <jni.h>
#include <android/log.h>
#include "xbee.h"


JNIEXPORT void JNICALL Java_com_utbm_lo52_chupachups_XBeeReader_initCOM(JNIEnv * env, jobject obj)
{
	initCOM();
    __android_log_print(ANDROID_LOG_DEBUG, "JNI XBEE", "COM INITIALISED");
}

JNIEXPORT jint JNICALL Java_com_utbm_lo52_chupachups_XBeeReader_readChar(JNIEnv * env, jobject obj)
{
	int i = readChar();
	return i;
}

JNIEXPORT void JNICALL Java_com_utbm_lo52_chupachups_XBeeReader_closeCOM(JNIEnv * env, jobject obj)
{
	closeCOM();
    __android_log_print(ANDROID_LOG_DEBUG, "JNI XBEE", "COM CLOSED");
}
