LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES := xbee_missile.c
LOCAL_C_INCLUDES += \
$(LOCAL_PATH)/../ \
$(LOCAL_PATH)/../../../mlbin/mlbin/


LOCAL_MODULE := xbee_missile
LOCAL_MODULE_TAGS := optional
LOCAL_SHARED_LIBRARIES := libxbee_shared libmlbin_shared

include $(BUILD_EXECUTABLE)



