#include <unistd.h>

#include "mlbin.h"
#include "xbee.h"


int
main (int argc, char *argv[])
{
	initCOM();
        mlbin_init_usb();
	mlbin_stop();
	
	for(;;){
		usleep(1E3*100);//100 [ms]
		unsigned char x = readChar();
        switch(x) {
			case 'L':
				mlbin_move_left();
				break;
			case 'R':
				mlbin_move_right();
				break;
			case 'U':
				mlbin_move_up();
				break;
			case 'D':
				mlbin_move_down();
				break;
			case 'F':
				mlbin_fire();
				break;
			default:
		                mlbin_stop();
				break;
		}
	}

	mlbin_free_usb();
	closeCOM();
	return 0;
}
