LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES := ../xbee.c
LOCAL_C_INCLUDES += \
$(LOCAL_PATH)/../


LOCAL_MODULE := libxbee_shared
LOCAL_MODULE_TAGS := optional
LOCAL_SHARED_LIBRARIES := libusb

include $(BUILD_SHARED_LIBRARY)


