int mlbin_init_usb(void);

int mlbin_free_usb(void);

int mlbin_fire(void);

int mlbin_move_down(void);

int mlbin_move_left(void);

int mlbin_move_right(void);

int mlbin_move_up(void);

int mlbin_stop(void);


