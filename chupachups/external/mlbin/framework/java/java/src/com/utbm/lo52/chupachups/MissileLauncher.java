package com.utbm.lo52.chupachups;

public class MissileLauncher {
	static {
		System.loadLibrary("mljni");
	}

	public native void initUsb();
	public native void freeUsb();
	public native void fire();
	public native void moveDown();
	public native void moveLeft();
	public native void moveRight();
	public native void moveUp();
	public native void stop();
}
