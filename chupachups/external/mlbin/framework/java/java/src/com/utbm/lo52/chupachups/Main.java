package com.utbm.lo52.chupachups;

public class Main {
	public static void main(String[] args) throws InterruptedException {
		MissileLauncher ml = new MissileLauncher();
		ml.initUsb();

		ml.moveLeft();
		Thread.sleep(1000);
		ml.stop();
		ml.moveRight();
		Thread.sleep(1000);
		ml.stop();
		ml.moveUp();
		Thread.sleep(1000);
		ml.stop();
		ml.moveDown();
		Thread.sleep(1000);
		ml.stop();
		ml.fire();
		Thread.sleep(1000);
		ml.stop();
		ml.fire();
		Thread.sleep(1000);
		ml.stop();
                ml.fire();
		Thread.sleep(1000);
		ml.stop();

                ml.freeUsb();
	}
}
