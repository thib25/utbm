LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES := $(call all-java-files-under, java)

LOCAL_MODULE_TAGS := optional

LOCAL_MODULE := com.utbm.lo52.chupachups.MissileLauncher

include $(BUILD_JAVA_LIBRARY)

