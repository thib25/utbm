LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE := com.utbm.lo52.chupachups.MissileLauncher.xml

LOCAL_MODULE_TAGS := optional

LOCAL_SRC_FILES := com.utbm.lo52.chupachups.MissileLauncher.xml

LOCAL_MODULE_CLASS := ETC

LOCAL_MODULE_PATH := $(TARGET_OUT_ETC)/permissions

include $(BUILD_PREBUILT)
