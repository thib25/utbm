LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES := mljni.c

LOCAL_C_INCLUDES += \
$(JNI_H_INCLUDE) \
$(LOCAL_PATH)/../mlbin

LOCAL_MODULE := libmljni

LOCAL_MODULE_TAGS := optional

LOCAL_SHARED_LIBRARIES := \
libutils \
libcutils\
libmlbin_shared

include $(BUILD_SHARED_LIBRARY)

