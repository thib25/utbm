LOCAL_PATH := $(call my-dir)

commonSources := \
	bmpfile.c \
	test.c

include $(CLEAR_VARS)

LOCAL_SRC_FILES := $(commonSources)

LOCAL_C_INCLUDES += external/libbmp

LOCAL_MODULE := libbmp

LOCAL_MODULE_TAGS := optional

include $(BUILD_SHARED_LIBRARY)

