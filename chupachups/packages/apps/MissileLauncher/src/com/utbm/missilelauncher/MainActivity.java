package com.utbm.missilelauncher;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ToggleButton;
import android.widget.CompoundButton;
import android.widget.Toast;
import android.util.Log;

import com.utbm.lo52.chupachups.*;


public class MainActivity extends Activity {

    private MissileLauncher ml;
    private XBeeReader xb;
    
    private Thread xbeeThread;
    private boolean xbeeThreadRun;
	
	private ToggleButton xbeeOnOff;

    @Override
    protected void onResume() {
	super.onResume();
	Log.d("MissileLauncherApp", "onResume start");
        ml.initUsb();
	Log.d("MissileLauncherApp", "onResume end");
    }

    @Override
    protected void onPause() {
	super.onPause();
	Log.d("MissileLauncherApp", "onPause start");
        if (xbeeOnOff.isChecked()) {
			xbeeThreadRun = false;
			try {
			    xbeeThread.join();
			} catch (InterruptedException e) {
			}
			xb.closeCOM();
			xbeeOnOff.setChecked(false);
		}
		ml.stop();
        ml.freeUsb();
	Log.d("MissileLauncherApp", "onPause end");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
	Log.d("MissileLauncherApp", "onCreate start");
        setContentView(R.layout.activity_main);

        // Récupération des boutons
        Button left = (Button) findViewById(R.id.move_left);
        Button right = (Button) findViewById(R.id.mov_right);
        Button down = (Button) findViewById(R.id.move_down);
        Button up = (Button) findViewById(R.id.move_up);

        Button fire = (Button) findViewById(R.id.fire);

	xbeeOnOff = (ToggleButton) findViewById(R.id.toggleButton);

	ml = new MissileLauncher();
	xb = new XBeeReader();

	xbeeOnOff.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			if(isChecked) {				
        			xb.initCOM();
				    xbeeThreadRun = true;
        			xbeeThread = new Thread(new Runnable() {
        			public void run() {
						try {
		    				while(xbeeThreadRun){
								Thread.sleep(100);//100 [ms]
								int readed = xb.readChar();
								Log.d("MissileLauncherApp", "char readed : "+readed);
								switch(readed) {
									case 76:
										ml.moveLeft();
										break;
									case 82:
										ml.moveRight();
										break;
									case 85:
										ml.moveUp();
										break;
									case 68:
										ml.moveDown();
										break;
									case 70:
										ml.fire();
										break;
									default :
										ml.stop();
								}
							}
						} catch (InterruptedException e) {

						}
					}
		    		});
				xbeeThread.start();
			} else {
				xbeeThreadRun = false;
				try {
				    xbeeThread.join();
				} catch (InterruptedException e) {
				}
				ml.stop();
				xb.closeCOM();
			}
		}	
	});


        // Move listeners
        left.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    ml.moveLeft();
                    showToast("left pressed");
                }
                else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    // button released
                    ml.stop();
                    showToast("left released");
                }

                return true;
            }
        });

        right.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    // button pressed
                    ml.moveRight();
                    showToast("right pressed");
                }
                else if(motionEvent.getAction() == MotionEvent.ACTION_UP){
                    // button released
                    ml.stop();
                    showToast("right released");
                }

                return true;
            }
        });


        down.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    // button pressed
                    ml.moveDown();
                    showToast("down pressed");
                } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    // button released
                    ml.stop();
                    showToast("down released");
                }

                return true;
            }
        });

        up.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    // button pressed
                    ml.moveUp();
                    showToast("up pressed");
                } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    // button released
                    ml.stop();
                    showToast("up released");
                }

                return true;
            }
        });


        // Rocket buttons listeners
        fire.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    // button pressed
                    ml.fire();
                    showToast("FIRE!");
                } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    // button released
                    ml.stop();
                }
                return true;
            }
        });
	Log.d("MissileLauncherApp", "onCreate end");
    }

    public void showToast(String msg){
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, msg, duration);
        toast.show();
    }
}
