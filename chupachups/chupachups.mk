$(call inherit-product, device/linaro/arrow/arrow.mk)

PRODUCT_PACKAGES += \
libbmp \
libusb \
mlbin_bin \
libmlbin_shared \
libmljni \
com.utbm.lo52.chupachups.MissileLauncher.xml \
com.utbm.lo52.chupachups.MissileLauncher \
com.utbm.lo52.chupachups.XBeeReader.xml \
com.utbm.lo52.chupachups.XBeeReader \
MissileLauncherApp \
xbee_bin \
libxbee_shared \
libxbeejni \
xbee_missile

DEVICE_PACKAGE_OVERLAYS := $(LOCAL_PATH)/overlay

PRODUCT_COPY_FILES += \
device/utbm/chupachups/system/media/bootanimation.zip:/system/media/bootanimation.zip \
device/utbm/chupachups/ueventd.chupachups.rc:root/ueventd.chupachups.rc

PRODUCT_NAME:=chupachups
PRODUCT_DEVICE:=chupachups
PRODUCT_BRAND:=Android
PRUDUCT_MODEL:=Strawberry
